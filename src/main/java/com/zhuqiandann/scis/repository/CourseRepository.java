package com.zhuqiandann.scis.repository;

import com.zhuqiandann.scis.model.Course;
import com.zhuqiandann.scis.model.Student;
import com.zhuqiandann.scis.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/16
 * @version: 1.0
 */
public interface CourseRepository  extends JpaRepository<Course,Integer> {

    /**
     * 根据周数查看课程
     * @param week
     * @return
     */
    List<Course> findAllByWeek(int week);

    /**
     * 根据课程名称查看课程
     * @param name
     * @return
     */
    Course findAllByName(String name);

    /**
     * 根据学生号查看课程
     * @param student
     * @return
     */
    List<Course> findByStudent(Student student);

    /**
     * 根据教师号查看课程
     * @param teacher
     * @return
     */
    List<Course> findByTeacher(Teacher teacher);

    /**
     * 模糊查看课程
     * @param name
     * @return
     */
    @Query(value = " select * from course where name like '%?1%' or num like '%?1%'", nativeQuery = true)
    List<Course> findByName(String name);

    List<Course> findByNum(Long num);
}
