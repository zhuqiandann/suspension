package com.zhuqiandann.scis.repository;

import com.zhuqiandann.scis.model.Paper;
import com.zhuqiandann.scis.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/21
 * @version: 1.0
 */
public interface PaperRepository  extends JpaRepository<Paper,Integer> {
    List<Paper> findByStudent(Student student);
    Paper findAdminByNum(Long num);
    Paper findAdminById(Integer id);
}
