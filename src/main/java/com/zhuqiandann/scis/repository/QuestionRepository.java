package com.zhuqiandann.scis.repository;

import com.zhuqiandann.scis.model.Paper;
import com.zhuqiandann.scis.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/21
 * @version: 1.0
 */
public interface QuestionRepository  extends JpaRepository<Question,Integer> {
    List<Question> findByPaper(Paper paper);
}
