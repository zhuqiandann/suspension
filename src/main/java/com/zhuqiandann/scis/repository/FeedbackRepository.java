package com.zhuqiandann.scis.repository;


import com.zhuqiandann.scis.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<Feedback,Integer> {


}
