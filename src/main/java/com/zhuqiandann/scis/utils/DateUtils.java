package com.zhuqiandann.scis.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/15
 * @version: 1.0
 */
public class DateUtils {
    public static String getTheDate(){
        Calendar date = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String time = format.format(date.getTime());
        return time;
    }



    public static String getDateFromTimestamp(String timestamp){
        long time  = Long.parseLong(timestamp);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(time);
        return sdf.format(date);
    }
}
