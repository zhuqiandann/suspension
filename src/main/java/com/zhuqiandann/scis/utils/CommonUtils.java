package com.zhuqiandann.scis.utils;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/5/20
 * @version: 1.0
 */

@SuppressWarnings("unchecked")
public class CommonUtils {

    /**
     * 判断字符串是否包含内容
     *
     */
    public static boolean isNotEmpty(String str) {
        return str != null && !str.trim().equals("");
    }

    /**
     * 判断是否为数字
     *
     * @param tg
     * @return
     */
    public static Boolean isNumber(String tg) {
        if (isNotEmpty(tg)) {
            try {
                Double.valueOf(tg);
                return true;
            } catch (NumberFormatException e) {
            }
        }
        return false;
    }

}
