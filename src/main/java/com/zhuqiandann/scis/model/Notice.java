package com.zhuqiandann.scis.model;

import javax.persistence.*;

/**
 * Created by jiac on 2018/4/10.
 * ClassName  : com.jcohy.scis.model
 * Description  :
 */
@Entity
@Table(name = "notice")
public class Notice {

    private static final long serialVersionUID = 9L;
    //Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "num")
    private Long num;

    @Column(name = "student_num")
    private Long studentNum;

    @Column(name = "course_num")
    private Long courseNum;


    @Column(name = "status")
    private String status;

    @Column(name = "content")
    private String content;

    @Column(name = "operation_date")
    private String date;

    @Column(name = "level")
    private Integer level;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Notice() {
    }

    public Notice(Long num,Long studentNum, Long courseNum,String status, String content, String date) {
        this.num = num;
        this.studentNum = studentNum;
        this.courseNum = courseNum;
        this.status = status;
        this.content = content;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(Long studentNum) {
        this.studentNum = studentNum;
    }

    public Long getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(Long courseNum) {
        this.courseNum = courseNum;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Notice{" +
                "num=" + num +
                ", studentNum=" + studentNum +
                ", courseNum=" + courseNum +
                ", content='" + content + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
