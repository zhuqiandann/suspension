package com.zhuqiandann.scis.model;

import javax.persistence.*;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/21
 * @version: 1.0
 */
@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "paper_id")
    private Paper paper;
    @Column(name = "create_time")
    private String createTime;
    @Column(name = "question_type")
    private Integer questionType;//问题类型：1：单选题2：多选题3：简答题
    @Column(name = "question_title")
    private String questionTitle;//问题标题
    @Column(name = "question_option")
    private String questionOption;// 问题的选项：1.选择题：[option1,option2,option3...]2.简答题：空字符串

    @Column(name = "question_answer")//同上
    private String questionAnswer;

    public String getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(String questionAnswer) {
        this.questionAnswer = questionAnswer;
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public Paper getPaper() {
        return paper;
    }

    public String getQuestionOption() {
        return questionOption;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public void setQuestionOption(String questionOption) {
        this.questionOption = questionOption;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }
}
