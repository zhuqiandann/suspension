package com.zhuqiandann.scis.model;

import javax.persistence.*;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/21
 * @version: 1.0
 * @update:2020/4/24
 */
@Entity
@Table(name = "paper")
public class Paper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "num")
    private Long num;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;
    @Column(name = "title")
    private String title;
    @Column(name = "create_date")
    private String createDate;
    @Column(name = "update_date")
    private String updateDate;
    @Column(name = "status")
    private Integer status;//状态值：0：未发布1：已发布2：已结束
    @Column(name = "start_time")
    private String startTime;//开始时间
    @Column(name = "end_time")
    private String endTime;//结束时间

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public Long getNum() {
        return num;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public Integer getId() {
        return id;
    }

    public Integer getStatus() {
        return status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getTitle() {
        return title;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public Student getStudent() {
        return student;
    }

}
