package com.zhuqiandann.scis.model;

import javax.persistence.*;

/**
 * @description:课程表
 * @author:ZhouDan
 * @date: 2020/3/16
 * @version: 1.0
 */
@Entity
@Table(name = "course")
public class Course {
    private static final long serialVersionUID = 14L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "num")
    private Long num;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "dept_id")
    private Dept dept;
    @Column(name = "begin_week")
    private Integer beginWeek;
    @Column(name = "end_week")
    private Integer endWeek;
    @Column(name = "week")
    private Integer week;
    @Column(name = "section")
    private String section;
    @Column(name = "person_time")
    private int personTime;
    @Column(name = "status")
    private int status;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    @Column(name = "create_date")
    private String createDate;

    @Column(name = "update_date")
    private String updateDate;

    public int getPersonTime() {
        return personTime;
    }

    public void setPersonTime(int personTime) {
        this.personTime = personTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public Integer getBeginWeek() {
        return beginWeek;
    }

    public void setBeginWeek(Integer beginWeek) {
        this.beginWeek = beginWeek;
    }

    public Integer getEndWeek() {
        return endWeek;
    }

    public void setEndWeek(Integer endWeek) {
        this.endWeek = endWeek;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", num=" + num +
                ", name='" + name + '\'' +
                ", beginWeek=" + beginWeek +
                ", endWeek=" + endWeek +
                ", week=" + week +
                ", section='" + section + '\'' +
                ", personTime=" + personTime +
                ", status=" + status +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                '}';
    }
}
