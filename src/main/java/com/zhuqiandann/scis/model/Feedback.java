package com.zhuqiandann.scis.model;


import javax.persistence.*;
import java.io.Serializable;

import java.util.Date;

/**
 * @description:异常反馈
 * @author:Zhangyuhui
 * @date: 2020/3/18
 * @version: 1.0
 */

@Entity
@Table(name = "feedback")
public class Feedback implements Serializable {

    private static final long serialVersionUID = 11L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "num")
    private Long num;

    @Column(name = "name")
    private String name;

    @Column(name = "week")
    private int week;

    @Column(name = "section")
    private String section;

    @Column(name = "teacher_name")
    private String teacherName;

    @Column(name = "update_date")
    private String updateDate;

    @Column(name = "description")
    private String description;

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
    public String getTeacherName(){return teacherName;}

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }
    public String getName(){
        return name;
    }
    public void setName(String n){
        this.name = n;
    }
    public int getWeek(){return week;}
    public void setWeek(int w){this.week = w;}
    public String getSection(){return section;}

    public void setSection(String Section) {
        this.section = Section;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    public String getUpdateDate(){
        return updateDate;
    }
}
