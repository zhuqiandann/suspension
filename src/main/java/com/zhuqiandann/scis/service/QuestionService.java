package com.zhuqiandann.scis.service;

import com.zhuqiandann.scis.model.Question;

import java.util.List;

public interface QuestionService {
    List<Question> findAll();

    void delete(Integer id);

    Question findById(Integer id);

    Question saveOrUpdate(Question obj)throws Exception;

    List<Question> findByPaper(Long num);

    List<Question> findByPaperid(Integer id);
}
