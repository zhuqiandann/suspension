package com.zhuqiandann.scis.service;

import com.zhuqiandann.scis.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/16
 * @version: 1.0
 */
public interface CourseService {
    /**
     * 查询所有课程
     * @return
     */
    List<Course> findAll();

    /**
     * 按周查看课程
     * @param week
     * @return
     */
    List<Course> findAllByWeek(int week);
    /**
     * 分页查询
     * @param pageable
     * @return
     */
    Page<Course> findAll(Pageable pageable);

    /**
     * 通过Id查找
     * @param id
     * @return
     */
    Course findById(Integer id);

    /**
     * 根据用户查询
     * @param num
     * @return
     */
    List<Course> findByStudent(Long num);

    /**
     * 根据老师查询
     * @param num
     * @return
     */
    List<Course> findByTeacher(Long num);

    /**
     * 增加，修改
     * @param course
     */
    Course saveOrUpdate(Course course) throws Exception;

    /**
     * 删除
     * @param id
     */
    void delete(Integer id);

    /**
     * 修改签到状态
     * @param id
     */
    void changeStatus(Integer id) throws Exception;

    /**
     * 签到人次
     * @param course
     */
    void changePersonTime(Course course) throws Exception;
    /**
     * 根据名称查询。
     * @param name
     * @return
     */
    Course findByName(String name);
    /**
     * 模糊查询。
     * @param name
     * @return
     */
    List<Course> findByNameLike(String name);

    List<Course> findByNum(Long num);
}
