package com.zhuqiandann.scis.service;

import com.zhuqiandann.scis.model.Paper;

import java.util.List;

public interface PaperService {


    List<Paper> findAll();

    void delete(Integer id);

    Paper findById(Integer id);

    Paper saveOrUpdate(Paper obj)throws Exception;

    List<Paper> findByStudent(Long num);

}
