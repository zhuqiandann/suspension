package com.zhuqiandann.scis.service.impl;

import com.jcohy.date.DateUtils;
import com.zhuqiandann.scis.model.Course;
import com.zhuqiandann.scis.repository.CourseRepository;
import com.zhuqiandann.scis.repository.StudentRepository;
import com.zhuqiandann.scis.repository.TeacherRepository;
import com.zhuqiandann.scis.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/16
 * @version: 1.0
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;
    @Override
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public List<Course> findAllByWeek(int week) {
        return courseRepository.findAllByWeek(week);
    }

    @Override
    public Page<Course> findAll(Pageable pageable) {
        return courseRepository.findAll(pageable);
    }

    @Override
    public Course findById(Integer id) {
        return courseRepository.findById(id).get();
    }

    @Override
    public List<Course> findByStudent(Long num) {
        return courseRepository.findByStudent(studentRepository.findAdminByNum(num));
    }

    @Override
    public List<Course> findByTeacher(Long num) {
        return courseRepository.findByTeacher(teacherRepository.findTeacherByNum(num));
    }

    @Override
    public Course saveOrUpdate(Course course) throws Exception {
        Course course1 = null;
        if(course.getId() != null){
            course1 = courseRepository.findById(course.getId()).get();
            if(course.getName() != null) {
                course1.setName(course.getName());
            }
            if(course.getName() != null) {
                course1.setName(course.getName());
            }
            if(course.getBeginWeek() != null) {
                course1.setBeginWeek(course.getBeginWeek());
            }
            if(course.getEndWeek() != null) {
                course1.setEndWeek(course.getEndWeek());
            }
            if(course.getSection() != null) {
                course1.setSection(course.getSection());
            }
            if(course.getWeek() != null) {
                course1.setWeek(course.getWeek());
            }
            course1.setDept(course.getDept());
            course1.setStudent(course.getStudent());
            course1.setTeacher(course.getTeacher());
            course1.setUpdateDate(DateUtils.getCurrentDateStr());
        }else{
            course1 = course;
            course1.setCreateDate(DateUtils.getCurrentDateStr());
        }
        return courseRepository.save(course1);
    }

    @Override
    public void delete(Integer id) {

        Course project = courseRepository.findById(id).get();
        courseRepository.delete(project);
    }

    @Override
    public void changeStatus(Integer id) throws Exception {
        Course course = courseRepository.findById(id).get();
        if(course.getStatus() == 1){
            course.setStatus(0);
        }else {
            course.setStatus(1);
        }
        courseRepository.save(course);
    }

    @Override
    public void changePersonTime(Course course) throws Exception {
        int persontime = course.getPersonTime();
        course.setPersonTime(persontime+1);
        courseRepository.save(course);
    }

    @Override
    public Course findByName(String name) {
        return courseRepository.findAllByName(name);
    }

    @Override
    public List<Course> findByNameLike(String name) {
        return courseRepository.findByName(name);
    }

    @Override
    public List<Course> findByNum(Long num) {
        return courseRepository.findByNum(num);
    }

}
