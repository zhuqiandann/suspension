package com.zhuqiandann.scis.service.impl;

import com.zhuqiandann.scis.model.Paper;
import com.zhuqiandann.scis.model.Question;
import com.zhuqiandann.scis.repository.PaperRepository;
import com.zhuqiandann.scis.repository.QuestionRepository;
import com.zhuqiandann.scis.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServicelmpl implements QuestionService {
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private PaperRepository paperRepository;

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        questionRepository.deleteById(id);
    }

    @Override
    public Question findById(Integer id) {
        return questionRepository.findById(id).get();
    }

    @Override
    public Question saveOrUpdate(Question obj) throws Exception {
         return questionRepository.save(obj);
    }

    @Override
    public List<Question> findByPaper(Long num) {
        Paper p = paperRepository.findAdminByNum(num);
        List<Question> ql = questionRepository.findByPaper(p);
        return ql;
    }

    @Override
    public List<Question> findByPaperid(Integer id) {
        Paper p = paperRepository.findAdminById(id);
        List<Question> ql = questionRepository.findByPaper(p);
        return ql;
    }
}
