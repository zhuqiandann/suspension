package com.zhuqiandann.scis.service.impl;

import com.zhuqiandann.scis.model.Feedback;
import com.zhuqiandann.scis.repository.FeedbackRepository;
import com.zhuqiandann.scis.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackRepository feedbackRepository;

    @Override
    public List<Feedback> findAll(){return feedbackRepository.findAll();}

    @Override
    public Page<Feedback>findAll(Pageable pageable){ return feedbackRepository.findAll(pageable); }
    @Override
    public void delete(Integer id) {
        feedbackRepository.deleteById(id);
    }

    @Override
    public Feedback saveOrUpdate(Feedback fb) throws Exception {
        return feedbackRepository.save(fb);
    }
    @Override
    public Feedback findById(Integer id){return feedbackRepository.findById(id).get();}


}
