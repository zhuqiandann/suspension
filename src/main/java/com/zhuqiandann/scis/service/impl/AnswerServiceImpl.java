package com.zhuqiandann.scis.service.impl;

import com.zhuqiandann.scis.model.Answer;
import com.zhuqiandann.scis.repository.AnswerRepository;
import com.zhuqiandann.scis.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author:Zhangyuhui
 * @date: 2020/4/23
 * @version: 1.0
 */
@Service
public class AnswerServiceImpl implements AnswerService {
    @Autowired
    private AnswerRepository answerRepository;

    @Override
    public List<Answer> findAll() {
        return answerRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        answerRepository.deleteById(id);
    }

    @Override
    public Answer findById(Integer id) {
        return answerRepository.findById(id).get();
    }

    @Override
    public Answer saveOrUpdate(Answer obj) throws Exception {
        return answerRepository.save(obj);
    }


}
