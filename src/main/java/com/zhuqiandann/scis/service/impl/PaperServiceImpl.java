package com.zhuqiandann.scis.service.impl;

import com.zhuqiandann.scis.model.Paper;
import com.zhuqiandann.scis.repository.PaperRepository;
import com.zhuqiandann.scis.repository.StudentRepository;
import com.zhuqiandann.scis.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author:Zhangyuhui
 * @date: 2020/4/23
 * @version: 1.0
 */
@Service
public class PaperServiceImpl implements PaperService{
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private PaperRepository paperRepository;

    @Override
    public List<Paper> findAll() {
        return paperRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        paperRepository.deleteById(id);
    }

    @Override
    public Paper findById(Integer id) {
        return paperRepository.findById(id).get();
    }

    @Override
    public Paper saveOrUpdate(Paper obj) throws Exception {
        return paperRepository.save(obj);
    }

    @Override
    public List<Paper> findByStudent(Long num) {
        return paperRepository.findByStudent(studentRepository.findAdminByNum(num));
    }

}
