package com.zhuqiandann.scis.service;

import com.zhuqiandann.scis.model.Feedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
public interface FeedbackService {
    List<Feedback> findAll();

    Page<Feedback> findAll(Pageable pageable);

    Feedback findById(Integer id);

    void delete(Integer id);

    Feedback saveOrUpdate(Feedback obj)throws Exception;
}
