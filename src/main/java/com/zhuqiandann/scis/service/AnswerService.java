package com.zhuqiandann.scis.service;

import com.zhuqiandann.scis.model.Answer;

import java.util.List;

public interface AnswerService {
    List<Answer> findAll();

    void delete(Integer id);

    Answer findById(Integer id);

    Answer saveOrUpdate(Answer obj)throws Exception;


}
