package com.zhuqiandann.scis.controller;

import com.zhuqiandann.scis.common.JsonResult;
import com.zhuqiandann.scis.common.PageJson;
import com.zhuqiandann.scis.model.Course;
import com.zhuqiandann.scis.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/16
 * @version: 1.0
 */
@Controller
@RequestMapping("/course")
public class CourseController extends BaseController{


    private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private CourseService courseService;

    @GetMapping("/list")
    @ResponseBody
    public PageJson<Course> all(ModelMap map){
        List<Course> courseList = courseService.findAll();
        PageJson<Course> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(courseList.size());
        page.setData(courseList);
        return page;
    }

    @GetMapping("/questionnaire")
    public String questionnaire(@RequestParam(required = false) Integer id, ModelMap map){
        return "/student/questionnaire";
    }
    /**g
     * 搜索模糊查询
     * @param keyword
     * @return
     */
    @PostMapping("/search")
    @ResponseBody
    public JsonResult searchCourse(String keyword){
        List<Course> courses = courseService.findByNameLike(keyword);
        System.out.println("=============================");
        courses.forEach(System.out::println);
        return JsonResult.ok().set("data", courses);
    }




}
