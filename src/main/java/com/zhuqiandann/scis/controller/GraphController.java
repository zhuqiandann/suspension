package com.zhuqiandann.scis.controller;

import com.zhuqiandann.scis.common.Data;
import com.zhuqiandann.scis.common.Graph;
import com.zhuqiandann.scis.common.JsonResult;
import com.zhuqiandann.scis.model.Answer;
import com.zhuqiandann.scis.model.Course;
import com.zhuqiandann.scis.model.Dept;
import com.zhuqiandann.scis.model.Type;
import com.zhuqiandann.scis.service.AnswerService;
import com.zhuqiandann.scis.service.CourseService;
import com.zhuqiandann.scis.service.DeptService;
import com.zhuqiandann.scis.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 23:50 2018/5/30
 * Email: jia_chao23@126.com
 * ClassName: GraphController
 * Description:
 **/
@RestController
@RequestMapping("/graph")
public class GraphController {


    @Autowired
    private CourseService courseService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private DeptService deptService;

    @Autowired
    private AnswerService answerService;


    @PostMapping("/type")
    public JsonResult graph() {
        Graph graph = new Graph();
        List<Course> courses = courseService.findAll();
        List<Answer> answers = answerService.findAll();
        List<Type> types = typeService.findAll();
        List<Dept> depts = deptService.findAll();
        List<Data> typeMap = new ArrayList<>();
        List<Data> deptMap = new ArrayList<>();
        List<Data> yearMap = new ArrayList<>();
        for (Type type : types) {
            Data data = new Data();
            List<Type> service = typeService.findAll();
            if (service.size() == 0) {
                data.setLabel(type.getName());
                data.setValue(0);
            } else {
                data.setLabel(type.getName());
                data.setValue(service.size());
            }
            typeMap.add(data);
        }


        for (Dept dept : depts) {
            Data data = new Data();
            List<Dept> service = deptService.findAll();
            if (service.size() == 0) {
                data.setLabel(dept.getName());
                data.setValue(0);
            } else {
                data.setLabel(dept.getName());
                data.setValue(service.size());
            }
            deptMap.add(data);
        }

        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        for (Answer answer : answers) {
            if (answer.getAnswerContent() == null) {
                count3++;
            } else if (answer.getAnswerContent().equals(answer.getQuestion().getQuestionAnswer())) {
                count1++;
            } else {
                count2++;
            }
        }
        yearMap.add(new Data("正确人数", count1));
        yearMap.add(new Data("错误人数", count2));
        yearMap.add(new Data("未填写", count3));

        graph.setType(typeMap);
        graph.setDeptMap(deptMap);
        graph.setYear(yearMap);
        System.out.println(graph);

        return JsonResult.ok("msg", graph);
    }
}
