package com.zhuqiandann.scis.controller;

import com.zhuqiandann.scis.common.JsonResult;
import com.zhuqiandann.scis.common.PageJson;
import com.zhuqiandann.scis.model.Course;
import com.zhuqiandann.scis.model.Dept;
import com.zhuqiandann.scis.model.Teacher;
import com.zhuqiandann.scis.service.CourseService;
import com.zhuqiandann.scis.service.DeptService;
import com.zhuqiandann.scis.service.StudentService;
import com.zhuqiandann.scis.service.TeacherService;
import com.zhuqiandann.scis.utils.ExportExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author:ZhouDan
 * @date: 2020/3/16
 * @version: 1.0
 */
@Controller
@RequestMapping("/admin/course")
public class AdminCourseController extends BaseController{

    @Autowired
    private CourseService courseService;

    @Autowired
    private DeptService deptService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @GetMapping("/list")
    @ResponseBody
    public PageJson<Course> all(ModelMap map){
        List<Course> courseList = courseService.findAll();
        PageJson<Course> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(courseList.size());
        page.setData(courseList);
        return page;
    }


    @GetMapping("/index")
    public String index(@RequestParam(required = false) Integer id, ModelMap map){
        return "admin/course/index";
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Integer id, ModelMap map){
        List<Dept> depts = deptService.findAll();
        map.put("depts",depts);
        List<Teacher> teachers = teacherService.findAll();
        map.put("teachers",teachers);
        if(id != null){
            Course course = courseService.findById(id);
            map.put("course",course);
        }
        return "admin/course/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public JsonResult save(Course course){
        try {
            if(course.getId() == null){
                List<Course> num = courseService.findByNum(course.getNum());
                if(num == null || num.size()>0){
                    return JsonResult.fail("此课程已存在");
                }
            }
            courseService.saveOrUpdate(course);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.fail(e.getMessage());
        }
        return JsonResult.ok();
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public JsonResult del(@PathVariable("id") Integer id){
        try {
            courseService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.fail("删除失败");
        }
        return JsonResult.ok();
    }

    @GetMapping("/exportExcel")
    @ResponseBody
    public JsonResult exportStatisticsReport(HttpServletRequest request, HttpServletResponse response,ModelMap map){
        List<Course> courses = courseService.findAll();
        //把要导出到excel的数据的LinkedHashMap装载到这个List里面,这是导出工具类要求封装格式.
        List<Map<String, Object>> exportData = new ArrayList<>();
        for(int i = 0 ; i< courses.size() ;i++){
            //使用LinkedHashMap,因为这个是有序的map
            LinkedHashMap<String,Object> reportData = new LinkedHashMap<>();
            //装载数据,就是要导出到excel的数据
            reportData.put("课程编号",courses.get(i).getNum());
            reportData.put("课程名称",courses.get(i).getName());
            reportData.put("所属院系",courses.get(i).getDept().getName());
            reportData.put("开始周",courses.get(i).getBeginWeek());
            reportData.put("结束周",courses.get(i).getEndWeek());
            reportData.put("星期",courses.get(i).getWeek());
            reportData.put("节数",courses.get(i).getSection());
            reportData.put("教师",courses.get(i).getTeacher().getName());
            reportData.put("累计人次",courses.get(i).getPersonTime());
            exportData.add(reportData);
        }

        //表格列名用ArrayList装载
        List<String> columns = new ArrayList<>();
        //设置excel表格中的列名
        columns.add("课程编号");
        columns.add("课程名称");
        columns.add("所属院系");
        columns.add("开始周");
        columns.add("结束周");
        columns.add("星期");
        columns.add("节数");
        columns.add("教师");
        columns.add("累计人次");

        String filename = "课程数据";
        try {
            response.setHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"), "ISO8859-1")+".xls");
        }catch (Exception e){
            e.printStackTrace();
            return JsonResult.fail("导出数据失败");
        }
        ExportExcel.exportToExcel(response, exportData, filename, columns);
        return JsonResult.ok();
    }

}
