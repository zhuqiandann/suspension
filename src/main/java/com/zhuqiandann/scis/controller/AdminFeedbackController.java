package com.zhuqiandann.scis.controller;

import com.zhuqiandann.scis.common.JsonResult;
import com.zhuqiandann.scis.common.PageJson;

import com.zhuqiandann.scis.model.Feedback;
import com.zhuqiandann.scis.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description:
 * @author:Zhangyuhui
 * @date: 2020/3/18
 * @version: 1.0
 */
@Controller
@RequestMapping("/admin/abnormal")
public class AdminFeedbackController extends BaseController{
    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("/list")
    @ResponseBody
    public PageJson<Feedback> all(ModelMap map){
        List<Feedback> FeedbackList = feedbackService.findAll();
        PageJson<Feedback> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(FeedbackList.size());
        page.setData(FeedbackList);
        return page;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Integer id, ModelMap map){

        if(id != null){
            Feedback feedback = feedbackService.findById(id);
            map.put("feedback",feedback);
        }
        return "admin/abnormal/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public JsonResult save(Feedback feedback){
        try {
            feedbackService.saveOrUpdate(feedback);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.fail(e.getMessage());
        }
        return JsonResult.ok();
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public JsonResult del(@PathVariable("id") Integer id){
        try {
            feedbackService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.fail("删除失败");
        }
        return JsonResult.ok();
    }


}
