package com.zhuqiandann.scis.controller;

import com.jcohy.date.DateUtils;
import com.jcohy.lang.StringUtils;
import com.zhuqiandann.scis.common.JsonResult;
import com.zhuqiandann.scis.common.PageJson;
import com.zhuqiandann.scis.model.*;
import com.zhuqiandann.scis.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ClassName: TeacherController
 * Description:
 **/
@Controller
@RequestMapping("/teacher")
public class TeacherController extends BaseController {

    @Autowired
    private StudentService studentService;


    @Autowired
    private TeacherService teacherService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private DeptService deptService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private NoticeService noticeService;

    @GetMapping("/project/list")
    @ResponseBody
    public PageJson<Project> all(@SessionAttribute("user") Teacher teacher, ModelMap map) {
//        Student student = studentService.findByNum(num);
        List<Project> projects = projectService.findByTeacher(teacher.getNum());
        PageJson<Project> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(projects.size());
        page.setData(projects);
        return page;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Integer id, ModelMap map) {
        List<Teacher> teachers = teacherService.findAll();
        map.put("teachers", teachers);
        if (id != null) {
            Project project = projectService.findById(id);
            map.put("project", project);
        }
        return "student/form";
    }

    @GetMapping("/list")
    @ResponseBody
    public PageJson<Teacher> teacher(@SessionAttribute("user") Teacher teacher, ModelMap map) {
        List<Teacher> teachers = teacherService.findAll();
        PageJson<Teacher> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(teachers.size());
        page.setData(teachers);
        return page;
    }

    @GetMapping("/search")
    @ResponseBody
    public PageJson search(String keyword, String dept) {
        List<Teacher> teachers = new ArrayList<>();
        PageJson<Teacher> page = new PageJson<>();

        if (StringUtils.isAllEmpty(keyword, dept)) {
            teachers = teacherService.findAll();
            page.setCode(0);
            page.setMsg("成功");
            page.setCount(teachers.size());
            page.setData(teachers);
            return page;
        }

        if (!StringUtils.isEmpty(keyword)) {
            boolean isNum = keyword.matches("[0-9]+");
            if (isNum) {
                Teacher teacher = teacherService.findByNum(Long.parseLong(keyword));
                if (dept != null && !dept.equals("")) {
                    if (teacher != null && teacher.getDept().getName().equals(dept)) {
                        teachers.add(teacher);
                    }
                } else {
                    teachers.add(teacher);
                }
            } else {
                Teacher teacher = teacherService.findByName(keyword);
                if (dept != null && !dept.equals("")) {
                    if (teacher.getDept().getName().equals(dept)) {
                        teachers.add(teacher);
                    }
                } else {
                    teachers.add(teacher);
                }
            }
        } else {
            List<Teacher> teacherList = teacherService.findAll();
            if (!StringUtils.isEmpty(dept)) {
                List<Teacher> list = teacherList.stream().filter(x -> x.getDept().getName().equals(dept)).collect(Collectors.toList());
                teachers = list;
            } else {
                teachers = teacherList;
            }
        }
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(teachers.size());
        page.setData(teachers);
        return page;
    }

    @GetMapping("/student")
    public String teacher(@RequestParam(required = false) Integer id, ModelMap map) {
        List<Dept> depts = deptService.findAll();
        map.put("depts", depts);
        return "student/student";
    }

    @GetMapping("/course")
    public String course(@RequestParam(required = false) Integer id, ModelMap map) {
        return "teacher/course";
    }

    @GetMapping("/course/list")
    @ResponseBody
    public PageJson<Course> allCourse(@SessionAttribute("user") Teacher teacher, ModelMap map) {
        List<Course> courses = courseService.findByTeacher(teacher.getNum());
        System.out.println(courses);
        PageJson<Course> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(courses.size());
        page.setData(courses);
        return page;
    }

    @PostMapping("/course/{id}/status")
    @ResponseBody
    public JsonResult changeStatus(@PathVariable("id") Integer id) throws Exception {
        Course course = courseService.findById(id);
        Notice notice = new Notice();
        if (course.getStatus() == 1) {
            course.setStatus(0);
            courseService.saveOrUpdate(course);
            notice.setCourseNum(course.getNum());
            notice.setDate(DateUtils.getCurrentDateStr());
            notice.setContent(course.getNum() + ":" + course.getName() + "退出了签到状态");
            notice.setLevel(2);
            noticeService.save(notice);
            return JsonResult.fail("已在签到状态中，正在退出签到状态");
        }
        try {
            courseService.changeStatus(id);
            notice.setCourseNum(course.getNum());
            notice.setDate(DateUtils.getCurrentDateStr());
            notice.setContent(course.getNum() + ":" + course.getName() + "发起了签到状态");
            notice.setLevel(2);
            noticeService.save(notice);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.fail("发起签到失败");
        }
        return JsonResult.ok();
    }


    @GetMapping("/notice/all")
    @ResponseBody
    public PageJson<Notice> notice(@SessionAttribute("user") Teacher teacher, ModelMap map) {
        List<Course> courses = courseService.findByTeacher(teacher.getNum());
        List<Notice> notices = new ArrayList<>();
        for (Course course : courses) {
            List<Notice> notices1 = noticeService.findByCourse(course.getNum());
            for (Notice notice : notices1) {
                if (notice != null) {
                    notices.add(notice);
                }
            }
        }
        List<Notice> noticeList = notices.stream().filter(x -> x.getLevel() <= 3).collect(Collectors.toList());
        PageJson<Notice> page = new PageJson<>();
        page.setCode(0);
        page.setMsg("成功");
        page.setCount(noticeList.size());
        page.setData(noticeList);
        return page;
    }

}
