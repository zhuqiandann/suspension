package com.zhuqiandann.scis.directive;

import com.zhuqiandann.scis.model.Paper;
import com.zhuqiandann.scis.service.PaperService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by jiac on 2018/5/3.
 * ClassName  : com.jcohy.recruit.directive
 * Description  :
 */
@Component
public class ProjectDirective implements TemplateDirectiveModel{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ProjectDirective.class);

    @Autowired
    private PaperService paperService;
    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        List<Paper> all = paperService.findAll();
        List<Paper> list = all.stream().filter(x -> x.getStatus() == 1).collect(Collectors.toList());
        logger.warn("Paper:{}",list.size());
        environment.setVariable("list", new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25).build().wrap(list));
        if (templateDirectiveBody != null) {
            templateDirectiveBody.render(environment.getOut());
        }
    }
}
