/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : scis

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 08/06/2020 10:26:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 1001, '管理员', '男', '123456', '123456');

-- ----------------------------
-- Table structure for allot
-- ----------------------------
DROP TABLE IF EXISTS `allot`;
CREATE TABLE `allot`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NULL DEFAULT NULL,
  `expert_id` int(11) NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `end` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `allot_project_id`(`project_id`) USING BTREE,
  INDEX `allot_expert_id`(`expert_id`) USING BTREE,
  CONSTRAINT `allot_expert_id` FOREIGN KEY (`expert_id`) REFERENCES `expert` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `allot_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of allot
-- ----------------------------
INSERT INTO `allot` VALUES (1, 1, 1, '1111', '2018-04-07', '2018-04-07');

-- ----------------------------
-- Table structure for answer
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `question_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `student_num` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `question_type` int(11) NOT NULL,
  `create_date` datetime(0) NOT NULL,
  `answer_content` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES (28, '1', '18', '1306', 1, '2020-04-26 14:59:35', '[\"好\"]', '2020-04-26 14:59:35');
INSERT INTO `answer` VALUES (29, '1', '5', '1306', 1, '2020-04-26 14:59:35', '[\"否\"]', '2020-04-26 14:59:35');
INSERT INTO `answer` VALUES (30, '2', '6', '1306', 1, '2020-05-08 19:59:17', '[\"0\"]', '2020-05-08 19:59:41');

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `upload_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `download_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES (1, '1', '上传地址', '下载地址', '1');

-- ----------------------------
-- Table structure for circular
-- ----------------------------
DROP TABLE IF EXISTS `circular`;
CREATE TABLE `circular`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告内容',
  `start` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `end` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结束时间',
  `visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0,不可见，1可见',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'url',
  `create_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `update_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `target_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '跳转url',
  `book_circular_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `book_circular_id`(`book_circular_id`) USING BTREE,
  CONSTRAINT `book_circular_id` FOREIGN KEY (`book_circular_id`) REFERENCES `book` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公告|jiac|20180117' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of circular
-- ----------------------------
INSERT INTO `circular` VALUES (1, '填写问卷', '2020-03-20 00:00:00', '2020-4-1', 1, 'https://www.wjx.cn/m/62274142.aspx', NULL, '2020-03-20 20:47:56', 'https://www.wjx.cn/m/62274142.aspx', NULL);

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `num` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dept_id` int(11) NULL DEFAULT NULL,
  `begin_week` int(11) NULL DEFAULT NULL,
  `week` int(11) NULL DEFAULT NULL,
  `end_week` int(11) NULL DEFAULT NULL,
  `section` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `student_id` int(11) NULL DEFAULT NULL,
  `teacher_id` int(11) NULL DEFAULT NULL,
  `person_time` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (00000000001, 1001, '软件工程', 1, 1, 2, 16, '10-12', '2020-03-17 17:27:08', '2020-05-21 11:15:03', 1, 1, 16, 1);
INSERT INTO `course` VALUES (00000000002, 1002, '高级软件工程', 1, 1, 2, 10, '10-12', '2020-03-20 15:19:57', '2020-03-20 16:08:12', 1, 2, 2, 0);
INSERT INTO `course` VALUES (00000000005, 1234, 'ads ', 4, 3, 2, 4, '8', '2020-04-25 17:53:39', '2020-04-28 11:20:43', 1, 2, 0, 0);

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `chairman` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, 1001, '计算机科学与工程', '小高', '123456789', '计算机', NULL, NULL);
INSERT INTO `dept` VALUES (2, 2001, '机械制造', '小倩', '12345678944', '机械制造学院', NULL, NULL);
INSERT INTO `dept` VALUES (3, 3001, '人文', '小段', '12345678944', '人文学院', NULL, NULL);
INSERT INTO `dept` VALUES (4, 1012, '软件工程', '小王', '15526172819', '软件工程', '2020-03-16 18:15:27', NULL);

-- ----------------------------
-- Table structure for expert
-- ----------------------------
DROP TABLE IF EXISTS `expert`;
CREATE TABLE `expert`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resume` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of expert
-- ----------------------------
INSERT INTO `expert` VALUES (1, 123456, '专家', '123456', '男', '2018-04-06', '123456', '专家地址', '专家简历');

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `num` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `week` int(11) NULL DEFAULT NULL,
  `section` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `teacher_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of feedback
-- ----------------------------
INSERT INTO `feedback` VALUES (10, '麦克风卡顿', '2020/3/21 下午10:09:20', '1002', '软件工程', 2, '10-12', '小张老师');

-- ----------------------------
-- Table structure for flyway_schema_history
-- ----------------------------
DROP TABLE IF EXISTS `flyway_schema_history`;
CREATE TABLE `flyway_schema_history`  (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `script` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `checksum` int(11) NULL DEFAULT NULL,
  `installed_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `installed_on` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`) USING BTREE,
  INDEX `flyway_schema_history_s_idx`(`success`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flyway_schema_history
-- ----------------------------
INSERT INTO `flyway_schema_history` VALUES (1, '1', '<< Flyway Baseline >>', 'BASELINE', '<< Flyway Baseline >>', NULL, 'root', '2020-03-16 16:29:32', 0, 1);
INSERT INTO `flyway_schema_history` VALUES (2, '1.0.0.20180406.1', 'SCIS DB INIT', 'SQL', 'V1.0.0_20180406_1__SCIS_DB_INIT.sql', 2041224549, 'root', '2020-03-16 16:29:32', 121, 1);
INSERT INTO `flyway_schema_history` VALUES (3, '1.0.0.20180407.1', 'ADD  TABLE ALLOTPRO', 'SQL', 'V1.0.0_20180407_1__ADD _TABLE_ALLOTPRO.sql', 1705450590, 'root', '2020-03-16 16:29:32', 16, 1);
INSERT INTO `flyway_schema_history` VALUES (4, '1.0.0.20180410.2', 'ADD  TABLE Notice', 'SQL', 'V1.0.0_20180410_2__ADD _TABLE_Notice.sql', -1226246389, 'root', '2020-03-16 16:29:32', 14, 1);
INSERT INTO `flyway_schema_history` VALUES (5, '1.0.0.20180416.1', 'ADD  TABLE FIELD', 'SQL', 'V1.0.0_20180416_1__ADD _TABLE_FIELD.sql', -768436933, 'root', '2020-03-16 16:29:32', 86, 1);
INSERT INTO `flyway_schema_history` VALUES (6, '1.0.0.20180416.2', 'ADD  SOME DATA', 'SQL', 'V1.0.0_20180416_2__ADD _SOME_DATA.sql', 335783523, 'root', '2020-03-16 16:29:32', 7, 1);
INSERT INTO `flyway_schema_history` VALUES (7, '1.0.0.20180510.1', 'ADD  TABLE CIRCULAR', 'SQL', 'V1.0.0_20180510_1__ADD _TABLE_CIRCULAR.sql', 1072634980, 'root', '2020-03-16 16:29:32', 9, 1);
INSERT INTO `flyway_schema_history` VALUES (8, '1.0.0.20180510.2', 'ADD  TABLE FIELD', 'SQL', 'V1.0.0_20180510_2__ADD _TABLE_FIELD.sql', 1904869983, 'root', '2020-03-16 16:29:32', 67, 1);
INSERT INTO `flyway_schema_history` VALUES (9, '1.0.0.20180510.3', 'ADD  TABLE FIELD', 'SQL', 'V1.0.0_20180510_3__ADD _TABLE_FIELD.sql', 1288629556, 'root', '2020-03-16 16:29:32', 14, 1);
INSERT INTO `flyway_schema_history` VALUES (10, '1.0.0.20180517.1', 'ADD  TABLE FIELD', 'SQL', 'V1.0.0_20180517_1__ADD _TABLE_FIELD.sql', 1928653640, 'root', '2020-03-16 16:29:32', 15, 1);
INSERT INTO `flyway_schema_history` VALUES (11, '1.0.0.20180517.2', 'ADD  FORGIN KEY', 'SQL', 'V1.0.0_20180517_2__ADD _FORGIN_KEY.sql', -2105509760, 'root', '2020-03-16 16:29:32', 30, 1);
INSERT INTO `flyway_schema_history` VALUES (12, '1.0.0.20180517.3', 'CHANGE  CIRCULAR FIELD', 'SQL', 'V1.0.0_20180517_3__CHANGE _CIRCULAR_FIELD.sql', -528529806, 'root', '2020-03-16 16:29:32', 76, 1);
INSERT INTO `flyway_schema_history` VALUES (13, '1.0.0.20180518.1', 'ADD PROJECT DATE', 'SQL', 'V1.0.0_20180518_1__ADD_PROJECT_DATE.sql', -2043696691, 'root', '2020-03-16 16:29:32', 86, 1);
INSERT INTO `flyway_schema_history` VALUES (14, '1.0.0.20180522.1', 'DELETE PROJECT FIELD', 'SQL', 'V1.0.0_20180522_1__DELETE_PROJECT_FIELD.sql', 366527784, 'root', '2020-03-16 16:29:32', 45, 1);
INSERT INTO `flyway_schema_history` VALUES (15, '1.0.0.20180524.1', 'CHANGE  NUM FIELD', 'SQL', 'V1.0.0_20180524_1__CHANGE _NUM_FIELD.sql', 176095671, 'root', '2020-03-16 16:29:32', 13, 1);
INSERT INTO `flyway_schema_history` VALUES (16, '1.0.0.20180525.1', 'ADD DEPT FIELD', 'SQL', 'V1.0.0_20180525_1__ADD_DEPT_FIELD.sql', 1519074102, 'root', '2020-03-16 16:29:33', 231, 1);
INSERT INTO `flyway_schema_history` VALUES (17, '1.0.0.20180527.1', 'ADD DEPT FIELD', 'SQL', 'V1.0.0_20180527_1__ADD_DEPT_FIELD.sql', 1240691303, 'root', '2020-03-16 16:29:33', 87, 1);

-- ----------------------------
-- Table structure for major
-- ----------------------------
DROP TABLE IF EXISTS `major`;
CREATE TABLE `major`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `assistant` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_id` int(11) NULL DEFAULT NULL,
  `create_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `major_dept_id`(`dept_id`) USING BTREE,
  CONSTRAINT `major_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of major
-- ----------------------------
INSERT INTO `major` VALUES (1, 100106, '软件工程', '123456789', '小高', 1, NULL, NULL);
INSERT INTO `major` VALUES (2, 100107, '信息工程', '123456789', '小倩', 1, NULL, NULL);
INSERT INTO `major` VALUES (3, 100103, '网络工程', '12345678944', '小胡', 1, NULL, NULL);
INSERT INTO `major` VALUES (4, 100104, '物联网', '12345678944', '小倩', 1, NULL, NULL);
INSERT INTO `major` VALUES (5, 200101, '机械制造', '12345678944', '小花', 2, NULL, NULL);
INSERT INTO `major` VALUES (6, 200102, '数控技术', '12345678944', '小资', 2, NULL, NULL);
INSERT INTO `major` VALUES (7, 200103, '铸造技术', '12345678944', '小南', 2, NULL, NULL);
INSERT INTO `major` VALUES (8, 300101, '中国语言文学', '12345678944', 'GodV', 3, NULL, NULL);
INSERT INTO `major` VALUES (9, 300102, '语言学及应用语言学', '12345678944', '小朱', 3, NULL, NULL);
INSERT INTO `major` VALUES (10, 300103, '汉语言文字学', '12345678944', '笑笑', 3, NULL, NULL);

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_num` int(11) NULL DEFAULT NULL,
  `project_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operation_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for paper
-- ----------------------------
DROP TABLE IF EXISTS `paper`;
CREATE TABLE `paper`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `student_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_date` datetime(0) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `start_time` datetime(0) NULL DEFAULT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `num`) USING BTREE,
  UNIQUE INDEX `num`(`num`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of paper
-- ----------------------------
INSERT INTO `paper` VALUES (1, 1001, NULL, '问卷', '2020-04-26 14:58:46', 1, '2020-04-24 14:00:48', '2020-04-24 14:00:50', '2020-04-26 14:58:46');
INSERT INTO `paper` VALUES (2, 344, NULL, '十大', '2020-04-25 11:34:35', 1, '2020-04-16 00:00:00', '2020-04-30 00:00:00', '2020-04-25 11:34:35');
INSERT INTO `paper` VALUES (6, 1003, NULL, '打赏 ', '2020-04-24 18:06:14', 1, '2020-04-15 00:00:00', '2020-04-30 00:00:00', '2020-04-24 18:06:14');
INSERT INTO `paper` VALUES (7, 1888, NULL, '是的', '2020-04-25 10:45:40', 1, '2020-04-14 00:00:00', '2020-04-30 00:00:00', '2020-04-25 10:45:40');

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pro_resource` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `money_resource` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `e_status` tinyint(1) NULL DEFAULT NULL,
  `e_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `student_id` int(11) NULL DEFAULT NULL,
  `teacher_id` int(11) NULL DEFAULT NULL,
  `book_id` int(11) NULL DEFAULT NULL,
  `t_status` tinyint(1) NULL DEFAULT NULL,
  `t_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `book_video_id` int(11) NULL DEFAULT NULL,
  `create_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新时间',
  `type_id` int(11) NULL DEFAULT NULL,
  `genre` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人/团体',
  `groups` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '团体参赛人编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pro_student_id`(`student_id`) USING BTREE,
  INDEX `pro_book_id`(`book_id`) USING BTREE,
  INDEX `pro_teacher`(`teacher_id`) USING BTREE,
  INDEX `book_video_id`(`book_video_id`) USING BTREE,
  INDEX `type_id`(`type_id`) USING BTREE,
  CONSTRAINT `book_video_id` FOREIGN KEY (`book_video_id`) REFERENCES `book` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pro_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pro_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pro_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `type_id` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES (1, '1', '项目来源', '经费来源', '项目描述', 1, '1', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `question_type` int(11) NOT NULL,
  `question_title` varchar(128) CHARACTER SET ucs2 COLLATE ucs2_general_ci NOT NULL,
  `question_option` varchar(512) CHARACTER SET ucs2 COLLATE ucs2_general_ci NOT NULL,
  `question_answer` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES (5, 1, '2020-04-26 14:59:17', 1, '你是不是傻', '[\"是\",\"否\"]', '[\"是\"]');
INSERT INTO `question` VALUES (6, 2, '2020-04-28 11:47:29', 1, '课程名称', '[\"软件工程技术与应用案例\",\"分布式数据库\"]', '[0]');
INSERT INTO `question` VALUES (7, 2, '2020-04-28 11:48:08', 1, '教师姓名', '[\"小王教师\",\"小张教师\"]', '[0]');
INSERT INTO `question` VALUES (9, 2, '2020-04-28 11:48:44', 1, '教师是否按时上课', '[\"是\",\"否\"]', '[1]');
INSERT INTO `question` VALUES (12, 2, '2020-04-25 22:32:59', 1, '有问题', '[\"有\",\"没有\"]', '[1]');
INSERT INTO `question` VALUES (18, 1, '2020-04-26 14:39:16', 1, '你好', '[\"好\",\"不好\"]', '[\"好\"]');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sclass` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `major_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `student_major_id`(`major_id`) USING BTREE,
  CONSTRAINT `student_major_id` FOREIGN KEY (`major_id`) REFERENCES `major` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, 1306, '我叫学生', '123456', '女', '2018-04-04', '1', '123456', 1);

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resume` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teacher_dept_id`(`dept_id`) USING BTREE,
  CONSTRAINT `teacher_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1, 1002, '小张老师', '123456', '女', '2018-04-04', '12345635423', '老师地址', '教授', '老师简历', 1);
INSERT INTO `teacher` VALUES (2, 1003, '小王老师', '123456', '女', '1980/01/30', '12321234567', '地址', '教师', '简历', 1);

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型名称',
  `num` int(11) NOT NULL,
  `number` int(11) NULL DEFAULT NULL COMMENT '类型拥有的app数量',
  `create_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES (1, '本科', 1001, 0, '2020-03-20 20:45:47', NULL);
INSERT INTO `type` VALUES (2, '研究生', 1002, 0, '2020-03-20 20:46:04', NULL);
INSERT INTO `type` VALUES (3, '交流生', 1003, 0, '2020-03-20 20:46:20', NULL);

SET FOREIGN_KEY_CHECKS = 1;
