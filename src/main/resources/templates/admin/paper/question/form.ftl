<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 标签信息</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link href="${ctx!}/js/plugins/layui/css/layui.css" rel="stylesheet" />
    <style type="text/css">
        .layui-form-item{
            margin: 50px 0 0 200px
        }

    </style>
</head>

<body>



<fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field">
    <legend style="text-align:center;">问题添加</legend>
</fieldset>
<form class="layui-form layui-form-pane" action="">

    <div class="layui-form-item">
        <input type="hidden" name="id" id="questionid" value="${(question.id)!}" >
    </div>

    <div class="layui-form-item">
        <input type="hidden" name="id" id="paperid" value="${(question.paper.id)!}" >
    </div>

    <div class="layui-form-item" style="margin-top: 10px">
        <label class="layui-form-label">题目类型</label>
        <div class="layui-input-inline">
            <input type="text" name="questionType" lay-verify="number"  placeholder="请输入题目类型" value="${(question.questionType)!}"
                   autocomplete="off" class="layui-input ">
        </div>
        <span style="color: red">必填 1：单选 2：多选 3：简答</span>
    </div>

    <div class="layui-form-item" style="margin-top: 10px">
        <label class="layui-form-label">题目标题</label>
        <div class="layui-input-inline">
            <input type="text" name="questionTitle" lay-verify="required"  placeholder="请输入问卷标题" value="${(question.questionTitle)!}"
                   autocomplete="off" class="layui-input ">
        </div>
        <span style="color: red">必填</span>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">选项</label>
        <div class="layui-input-inline">
            <input type="text" name="questionOption" lay-verify="required"  placeholder="请输入选项" value="${(question.questionOption)!}"
                   autocomplete="off" class="layui-input">
        </div>
        <span style="color: red">选择题：[option1,option2,option3...]简答题空</span>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">答案</label>
        <div class="layui-input-inline">
            <input type="text" name="questionAnswer" lay-verify="required" placeholder="答案" value="${(question.questionAnswer)!}"
                   autocomplete="off" class="layui-input">
        </div>
        <span style="color: #ff0000">[Aindex1,Aindex2,...]选项索引</span>
    </div>


    <div class="layui-form-item">
        <button class="layui-btn" lay-submit lay-filter="add">立即提交</button>
    </div>
</form>


<script src="${ctx!}/js/plugins/layui/layui.js"></script>
<script src="${ctx!}/js/common.js"></script>
<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.config({
        base: '${ctx}/js/admin/'
    }).use('paper/question/form');

</script>
<div id="google_translate_element" style="position:absolute;bottom:10px;right:10px;z-index:2000;opacity:0.7"></div>
<script>
    function googleTranslateElementInit() {

        new google.translate.TranslateElement(
            {

                includedLanguages: 'en,zh-CN,hr,cs,da,nl,fr,de,el,iw,hu,ga,it,ja,ko,pt,ro,ru,sr,es,th,vi',

                layout: google.translate.TranslateElement.InlineLayout.SIMPLE,

                autoDisplay: true,

            },
            'google_translate_element'//触发按钮的id
        );

    }
</script>
<script src="https://translate.google.cn/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>

</html>
