<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>数据列表页面</title>
    <!-- layui.css -->
    <link href="${ctx!}/js/plugins/layui/css/layui.css" rel="stylesheet" />
    <style>
        tr td:not(:nth-child(0)),
        tr th:not(:nth-child(0)) {
            text-align: center;
        }
        /*可选*/
        .layui-laypage > * {
            float: left;
        }
        .layui-field-title .layui-field-box{
            padding: 10px 20px 10px 30px;
        }
        .layui-table-cell{
            padding-top: 4px;
            height: 45px;
        }
        .star-so{
            text-align: center;
            margin-bottom: 10px;
            margin-top: 40px;
        }
        .star-so input.layui-input{
            width: 200px;
            display: inline-block;
        }

    </style>
</head>
<body>

<fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field">
    <legend style="text-align:center;">课程信息</legend>

    <button class="layui-btn" style="position: relative;float: right;right: 100px;" onclick="javascript:location.replace(location.href)">
        <i class="layui-icon">&#x1002;</i>
    </button>
    <div class="layui-row">
        <div class="layui-form layui-col-md12 star-so">
            <input class="layui-input" placeholder="请输入课程名称或编号" name="keyword" id="keyword">
            <button class="layui-btn" id="search" ><i class="layui-icon">&#xe615;</i></button>

        </div>
    </div>
    <div class="layui-field-box">
        <div id="dataContent">
            <table class="layui-hide" id="course" lay-filter="table"></table>
            <script type="text/html" id="operator">
                <a class="layui-btn layui-btn-normal" lay-event="sign1">发起签到</a>
                <a class="layui-btn layui-btn-normal" lay-event="error">异常反馈</a>
            </script>
        </div>
    </div>
</fieldset>
<!-- layui.js -->
<script src="${ctx!}/js/plugins/layui/layui.js"></script>

<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.define([ 'layer',  'table','form'], function (exports) {
        var $ = layui.jquery,
            form = layui.form,
            table  = layui.table ;

        var dept,keyword,major='';

        table.render({
            elem: '#course'
            ,height: 'full-200'
            ,method:'GET'
            ,url: '/teacher/course/list' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
               {field: 'num', align:'center', title: '编号',width:'7%',unresize:true}
                ,{field: 'name', align:'center', title: '课程名称',width:'8%',unresize:true}
                ,{field: 'dept', align:'center', title: '所属院系',width:'15%',unresize:true,templet:'<div>{{d.dept.name}}</div>'}
                ,{field: 'beginWeek', align:'center', title: '开始周',width:'10%',unresize:true}
                ,{field: 'endWeek', align:'center', title: '结束周',width:'10%',unresize:true}
                ,{field: 'week', align:'center', title: '星期',width:'5%',unresize:true}
                ,{field: 'section', align:'center', title: '节数',width:'10%',unresize:true}
                ,{field: 'createDate', title: '创建日期',align:'center',width:'15%', unresize: true}
                ,{field: 'personTime', title: '签到人数',align:'center',width:'8%', unresize: true}
                ,{fixed: 'right', title: '操作', toolbar: '#operator', width:'20%', align:'center', unresize: true}

            ]]
        });
        table.on('tool(table)', function(obj){
            var data = obj.data;
            if(obj.event === 'error'){
                layui.layer.open({
                    type:1,
                    title:'简述异常情况',
                    area:['500px','300px'],
                    btn:['确定','取消'],
                    content:'<div><textarea type="text"  autocomplete="off" class="layui-textarea" lay-verify="required" style="height:200px"></textarea></div>',
                    btn1:function (index,layero) {
                        var description = layero.find('textarea')[0].value;
                        var savedata = {};
                        var myDate = new Date();
                        var mytime=myDate.toLocaleString();
                        savedata["description"] = description;
                        savedata["teacherName"] = data.teacher.name;
                        savedata["name"] = data.name;
                        savedata["num"] = data.teacher.num;
                        savedata["week"] = data.week;
                        savedata["section"] = data.section;
                        savedata["updateDate"] = mytime;
                        layui.jquery.ajax ({
                            type: "POST",
                            dataType: "json",
                            url: "/admin/abnormal/save",
                            data: savedata,
                            success:function(result) {
                                if (result.isOk) {
                                    layui.layer.close(index);
                                    layer.msg("操作成功", {time: 2000});
                                } else {
                                    layui.layer.close(index);
                                    layer.msg(result.msg,{anim:6});
                                }
                            }
                        });

                    },
                    btn2:function (index,layero) {
                        layui.layer.close(index);

                    }

                })
            }

            if(obj.event === 'sign1'){
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "/teacher/course/"+ data.id +"/status",
                        success: function (ret) {
                            if (ret.isOk) {
                                layer.msg("操作成功", {time: 2000}, function () {
                                    layer.close(index);
                                    window.location.href = "/teacher/course";
                                });
                            } else {
                                layer.msg(ret.msg, {time: 2000});
                            }
                        }
                    });
            }
        });
        // form.on('select(depts)', function(data) {
        //     var select = $("[name='major']");
        //     dept = data.value;
        //     select.empty();
        //     $.ajax({
        //         type: "GET",
        //         data: {"id": data.value},
        //         url: "/major/dept",
        //         async: false,
        //         success: function (ret) {
        //             var option = '';
        //             var data = ret.majors;
        //             for (var i = 0; i < data.length; i++) {
        //                 option += '<option value="' + data[i].name + '">' + data[i].name + '</option></br>';
        //             }
        //             select.append(option);
        //             form.render('select', 'form');
        //         }
        //     });
        //
        // });
        //搜索
        $('#search').click(function () {
            keyword = $("#keyword").val();
            table.reload('course', {
                url: "/course/search"
                ,where: {keyword:keyword} //设定异步数据接口的额外参数
                // ,where: {keyword:keyword,dept:dept,major:major} //设定异步数据接口的额外参数
                //,height: 300
            });
        });

        // form.on('select(majors)', function(data){
        //     major = data.value;
        // });
    });
</script>
</body>
</html>