<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>数据列表页面</title>
    <!-- layui.css -->
    <link href="${ctx!}/js/plugins/layui/css/layui.css" rel="stylesheet" />
    <style>
        tr td:not(:nth-child(0)),
        tr th:not(:nth-child(0)) {
            text-align: center;
        }
        /*可选*/
        .layui-laypage > * {
            float: left;
        }
        .layui-field-title .layui-field-box{
            padding: 10px 20px 10px 30px;
        }
        .layui-table-cell{
            padding-top: 4px;
            height: 45px;
        }
        .star-so{
            text-align: center;
            margin-bottom: 10px;
            margin-top: 40px;
        }
        .star-so input.layui-input{
            width: 200px;
            display: inline-block;
        }
        img {
            vertical-align:middle;
        }

    </style>
</head>
<body>

<fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field">
    <legend style="text-align:center;">课程信息</legend>

    <button class="layui-btn" style="position: relative;float: right;right: 100px;" onclick="javascript:location.replace(location.href)">
        <i class="layui-icon">&#x1002;</i>
    </button>
    <div class="layui-row">
        <div class="layui-form layui-col-md12 star-so">
            <input class="layui-input" placeholder="请输入课程名称或编号" name="keyword" id="keyword">
            <button class="layui-btn" id="search" ><i class="layui-icon">&#xe615;</i></button>

        </div>
    </div>

    <div class="layui-field-box">
        <div id="dataContent" class="">
            <table class="layui-hide" id="course" lay-filter="table"></table>
            <script type="text/html" id="operator">
                <a class="layui-btn layui-btn-normal" lay-event="sign">签到</a>
            </script>
        </div>
    </div>
</fieldset>
<!-- layui.js -->
<script src="${ctx!}/js/plugins/layui/layui.js"></script>
<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.define([ 'layer',  'table','form'], function (exports) {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;

        var dept, keyword, major = '';

        table.render({
            elem: '#course'
            , height: 'full-200'
            , method: 'GET'
            , url: '/student/course/list' //数据接口
            , page: true //开启分页
            , cols: [[ //表头
                {field: 'num', align: 'center', title: '编号', width: '7%', unresize: true}
                , {field: 'name', align: 'center', title: '课程名称', width: '8%', unresize: true}
                , {
                    field: 'dept',
                    align: 'center',
                    title: '所属院系',
                    width: '15%',
                    unresize: true,
                    templet: '<div>{{d.dept.name}}</div>'
                }
                , {field: 'beginWeek', align: 'center', title: '开始周', width: '10%', unresize: true}
                , {field: 'endWeek', align: 'center', title: '结束周', width: '10%', unresize: true}
                , {field: 'week', align: 'center', title: '星期', width: '10%', unresize: true}
                , {field: 'section', align: 'center', title: '节数', width: '10%', unresize: true}
                , {field: 'createDate', title: '创建日期', align: 'center', width: '20%', unresize: true}
                , {fixed: 'right', title: '操作', toolbar: '#operator', width: '10%', align: 'center', unresize: true}
            ]]
        });

        //搜索
        $('#search').click(function () {
            keyword = $("#keyword").val();
            table.reload('course', {
                url: "/course/search"
                , where: {keyword: keyword} //设定异步数据接口的额外参数
                // ,where: {keyword:keyword,dept:dept,major:major} //设定异步数据接口的额外参数
                //,height: 300
            });
        });
        table.on('tool(table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'questionnaire') {
                layui.layer.open({
                    type: 1,
                    title: '学生问卷(扫码填写）',
                    area: ['200px', '200px'],
                    content: '<img src="/images/studentQN.jpg" >',

                })
                }
            if(obj.event === 'sign'){
                var select = $("[name='major']");
                dept = data.value;
                select.empty();
                $.ajax({
                    type: "POST",
                    dataType : "json",
                    url: "/student/course/"+data.id+"/sign",
                    success: function (ret) {
                        if (ret.isOk) {
                            layer.msg("操作成功", {time: 2000}, function () {
                                layer.close(index);
                                window.location.href = "/student/course/list";
                            });
                        } else {
                            layer.msg(ret.msg, {time: 2000});
                        }
                        }
                    });
            }
        });
    });

</script>


</body>
</html>