<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>数据列表页面</title>
    <!-- layui.css -->
    <link href="${ctx!}/js/plugins/layui/css/layui.css" rel="stylesheet" />
    <style>

        tr td:not(:nth-child(0)),
        tr th:not(:nth-child(0)) {
            text-align: center;
        }

        /*可选*/
        .layui-laypage > * {
            float: left;
        }
        .layui-field-title .layui-field-box{
            padding: 10px 0px 10px 30px;
        }
        .layui-table-cell{
            padding-top: 4px;
            height: 45px;
        }

    </style>
</head>
<body>

<fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field">
    <legend style="text-align:center;">问卷列表</legend>

    <div class="layui-field-box">
        <div id="dataContent" class="">
            <table class="layui-hide" id="paper" lay-filter="table"></table>
            <script type="text/html" id="operator">
                <a class="layui-btn" lay-event="do">查看填写</a>
            </script>
        </div>
    </div>
</fieldset>

<!-- layui.js -->
<script src="${ctx!}/js/plugins/layui/layui.js"></script>

<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.define(['laypage', 'layer',  'table','util','form'], function (exports) {
        var $ = layui.jquery,
            layer = layui.layer,
            util = layui.util,
            laypage = layui.laypage,
            form = layui.form,
            table  = layui.table ;
        table.render({
            elem: '#paper'
            ,height: 'full-200'
            ,method:'GET'
            ,url: '/student/paper/list' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {type: 'checkbox', align:'center',unresize:true}
                ,{field: 'num', align:'center', title: '问卷编号',unresize:true}
                ,{field: 'title', align:'center', title: '问卷名称',unresize:true}
                ,{field: 'createDate', align:'center', title: '创建时间',unresize:true}
                ,{field: 'updateDate', align:'center', title: '更新时间',unresize:true}
                ,{field: 'status', align:'center', title: '发布状态',unresize:true}
                ,{field: 'startTime', align:'center', title: '问卷开始时间',unresize:true}
                ,{field: 'endTime', align:'center', title: '问卷结束时间',unresize:true}
                ,{fixed: 'right',  title:'操作',align:'center', toolbar: '#operator',unresize:true}
            ]]
        });
        //监听工具条
        table.on('tool(table)', function(obj){
            if(obj.event === 'do'){
                var data = obj.data;
                    w=($(window).width()*0.9);
                    h=($(window).height() - 50);
                    layer.open({
                        type: 2,
                        area: [w+'px', h +'px'],
                        fix: false, //不固定
                        maxmin: true,
                        shadeClose: true,
                        shade:0.4,
                        title: "答题",
                        content:'question',
                        success:function (layero,index) {
                            var body = layer.getChildFrame('body',index);
                            body.contents().find("#paperid").val(data.id);
                        }
                });
            }
        });
    });

</script>
</body>
</html>