<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 标签信息</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link href="${ctx!}/js/plugins/layui/css/layui.css" rel="stylesheet" />
    <style type="text/css">
        .layui-form-item{
            margin: 50px 0 0 200px
        }

    </style>
</head>

<body>

<div class="layui-form-item">
    <input type="hidden" name="id" id="paperid" value="${(paper.id)!}" >
</div>

<form class="layui-form" id="question" action="">

    <div class="layui-form-item" style="margin-top: 10px">
        <label class="layui-form-label">学号</label>
        <div class="layui-input-inline">
            <input type="text" name="num" lay-verify="number"  placeholder="请输入学号" value="${(answer.num)!}"
                   autocomplete="off" class="layui-input ">
        </div>
        <span style="color: red">必填 例：1000</span>
    </div>
    <div class="layui-form-item" style="margin-top: 10px">
        <button class="layui-btn" lay-submit lay-filter="add">立即提交</button>
    </div>
</form>

<div id="google_translate_element" style="position:absolute;bottom:10px;right:10px;z-index:2000;opacity:0.7"></div>
<script>
    function googleTranslateElementInit() {

        new google.translate.TranslateElement(
            {

                includedLanguages: 'en,zh-CN,hr,cs,da,nl,fr,de,el,iw,hu,ga,it,ja,ko,pt,ro,ru,sr,es,th,vi',

                layout: google.translate.TranslateElement.InlineLayout.SIMPLE,

                autoDisplay: true,

            },
            'google_translate_element'//触发按钮的id
        );

    }
</script>
<!--
<script src="https://translate.google.cn/translate_a/element.js?cb=googleTranslateElementInit"></script>
-->
<script src="${ctx!}/js/plugins/layui/layui.js"></script>
<script src="${ctx!}/js/common.js"></script>
<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.define(['laypage', 'layer',  'table','util','form'], function (exports) {
        var $ = layui.jquery,
            layer = layui.layer,
            util = layui.util,
            laypage = layui.laypage,
            form = layui.form,
            table  = layui.table ;
        var paperid = $('#paperid').val();
        var questionlist;
        function maketitle(questionTitle){
            return '<label class="layui-form-label">'+questionTitle+'</label>'
        }

        function makeopt(questionOption){
            var str="";
            for(var i=0;i<questionOption.length;i++){
                str = str+'<input type="checkbox" name="'+questionOption[i]+'" lay-skin="primary" title="'+questionOption[i]+'">'
            }
            return str;
        }

        $.get("paper/question?id="+paperid,function(ret,status){//paperid
            questionlist = ret.data;
            for(var i=0;i<questionlist.length;i++) {
                questionlist[i].questionOption = eval(questionlist[i].questionOption);
                questionlist[i].questionAnswer = eval(questionlist[i].questionAnswer);
                var html = '<div class="layui-form-item" pane="">'+maketitle(questionlist[i].questionTitle)
                    +'<div class="layui-input-block">'+makeopt(questionlist[i].questionOption)
                    +'</div>'
                    +'</div>'
                $('#question').append(html);
            }
            form.render();

        });

        Date.prototype.Format = function(fmt)
        { //author: meizz
            var o = {
                "M+" : this.getMonth()+1,                 //月份
                "d+" : this.getDate(),                    //日
                "h+" : this.getHours(),                   //小时
                "m+" : this.getMinutes(),                 //分
                "s+" : this.getSeconds(),                 //秒
                "q+" : Math.floor((this.getMonth()+3)/3), //季度
                "S"  : this.getMilliseconds()             //毫秒
            };
            if(/(y+)/.test(fmt))
                fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
            for(var k in o)
                if(new RegExp("("+ k +")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
            return fmt;
        }


        //监听登陆提交
        form.on('submit(add)', function (data) {
            console.log(data.elem);//被执行事件的元素DOM对象，一般为button对象
            console.log(data.form);//被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field);//当前容器的全部表单字段，名值对形式：{name: value}
            var answer = new Array();
            for(var key in data.field){
                answer.push(key);
            }
            for(var i=0;i<questionlist.length;i++){
                mapdata={};
                mapdata.paper = paperid;
                mapdata.question = questionlist[i].id;
                mapdata.num = data.field.num;
                mapdata.questionType = questionlist[i].questionType;
                mapdata.createDate = (new Date()).Format("yyyy-MM-dd hh:mm:ss");
                mapdata.updateDate = (new Date()).Format("yyyy-MM-dd hh:mm:ss");
                mapdata.answerContent = '["'+answer[i+1]+'"]';
                console.log(mapdata);
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "/admin/answer/save",
                    data: mapdata,
                    success: function(ret){
                        if(ret.isOk){
                            layer.msg("操作成功",{time:2000});
                        }else{
                            layer.msg(ret.msg, {time: 2000});
                        }
                    }
                });
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
                layer.msg("填完",{time:2000});
            }

        });
    });

</script>

</body>

</html>
