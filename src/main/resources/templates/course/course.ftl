<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>数据列表页面</title>
    <!-- layui.css -->
    <link href="${ctx!}/js/plugins/layui/css/layui.css" rel="stylesheet" />
    <style>
        tr td:not(:nth-child(0)),
        tr th:not(:nth-child(0)) {
            text-align: center;
        }
        /*可选*/
        .layui-laypage > * {
            float: left;
        }
        .layui-field-title .layui-field-box{
            padding: 10px 20px 10px 30px;
        }
        .layui-table-cell{
            padding-top: 4px;
            height: 45px;
        }
        .star-so{
            text-align: center;
            margin-bottom: 10px;
            margin-top: 40px;
        }
        .star-so input.layui-input{
            width: 200px;
            display: inline-block;
        }

    </style>
</head>
<body>

<fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field">
    <legend style="text-align:center;">课程信息</legend>

    <button class="layui-btn" style="position: relative;float: right;right: 100px;" onclick="javascript:location.replace(location.href)">
        <i class="layui-icon">&#x1002;</i>
    </button>
    <div class="layui-row">
        <div class="layui-form layui-col-md12 star-so">


            <input class="layui-input" placeholder="请输入课程名称" name="keyword" id="keyword">

            <div class="layui-input-inline">
                <select name="displayName" id="dept" lay-filter="dept" placeholder="请选择院系">
                    <option value="">请选择院系</option>
                    <#list depts as x>
                        <option >${x.name}</option>
                    </#list>
                </select>
            </div>

            <button class="layui-btn" id="search" ><i class="layui-icon">&#xe615;</i></button>
        </div>
    </div>
    <div class="layui-field-box">
        <div id="dataContent">
            <table class="layui-hide" id="teacherinfo" lay-filter="table"></table>
            <script type="text/html" id="operator">
                <a class="layui-btn layui-btn-normal" lay-event="detail">查看详情</a>
            </script>
        </div>
    </div>
</fieldset>
<!-- layui.js -->
<script src="${ctx!}/js/plugins/layui/layui.js"></script>
<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.define([ 'layer',  'table','form'], function (exports) {
        var $ = layui.jquery,
            form = layui.form,
            table  = layui.table ;

        var dept,keyword='';

        table.render({
            elem: '#teacherinfo'
            ,height: 'full-200'
            ,method:'GET'
            ,url: 'student/course/list' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {type: 'checkbox', align:'center',unresize:true}
                ,{field: 'num', align:'center', title: '课程编号',unresize:true}
                ,{field: 'name', align:'center', title: '课程名称',unresize:true}
                ,{field: 'dept', align:'center', title: '所属院系',unresize:true,templet:'<div>{{d.dept.name}}</div>'}
                ,{field: 'beginWeek', align:'center', title: '开始周',unresize:true}
                ,{field: 'endWeek', align:'center', title: '结束周',unresize:true}
                ,{field: 'week', align:'center', title: '星期',unresize:true}
                ,{field: 'section', align:'center', title: '节数',unresize:true}
            ]]
        });
        //搜索
        $('#search').click(function () {
            keyword = $("#keyword").val();
            table.reload('course', {
                url: "/course/search"
                ,where: {keyword:keyword,dept:dept} //设定异步数据接口的额外参数
                //,height: 300
            });
        });


        form.on('select(dept)', function(data){
            dept = data.value;
        });

    });
</script>
<div id="google_translate_element" style="position:absolute;bottom:10px;right:10px;z-index:2000;opacity:0.7"></div>
<script>
    function googleTranslateElementInit() {

        new google.translate.TranslateElement(
            {

                includedLanguages: 'en,zh-CN,hr,cs,da,nl,fr,de,el,iw,hu,ga,it,ja,ko,pt,ro,ru,sr,es,th,vi',

                layout: google.translate.TranslateElement.InlineLayout.SIMPLE,

                autoDisplay: true,

            },
            'google_translate_element'//触发按钮的id
        );

    }
</script>
<script src="https://translate.google.cn/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>
</html>