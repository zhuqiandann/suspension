layui.define([ 'layer',  'table','common'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        common = layui.common,
        table  = layui.table ;
    table.render({
        elem: '#course'
        ,height: 'full-200'
        ,method:'GET'
        ,url: '/student/course/list' //数据接口
        ,page: true //开启分页
        ,cols: [[ //表头
            {type: 'checkbox', align:'center',unresize:true}
            ,{field: 'num', align:'center', title: '课程编号',unresize:true}
            ,{field: 'name', align:'center', title: '课程名称',unresize:true}
            ,{field: 'dept', align:'center', title: '所属院系',unresize:true,templet:'<div>{{d.dept.name}}</div>'}
            ,{field: 'beginWeek', align:'center', title: '开始周',unresize:true}
            ,{field: 'endWeek', align:'center', title: '结束周',unresize:true}
            ,{field: 'week', align:'center', title: '星期',unresize:true}
            ,{field: 'teacher', align:'center', title: '教师',unresize:true,templet:'<div>{{d.dept.teacher.name}}</div>'}
            ,{field: 'createDate', title: '创建日期',unresize:true}
            ,{fixed: 'right',  title:'操作',align:'center', toolbar: '#operator',unresize:true}
        ]]
    });

    table.on('tool(table)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            del(data.id);
        } else if(obj.event === 'edit'){
            common.frame_show('分类编辑','https://www.wjx.cn/m/62274142.aspx');
        }
    });

    exports('student/course', datalist);
});