layui.define(['laypage', 'layer',  'table','common','util','form'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        util = layui.util,
        laypage = layui.laypage,
        form = layui.form,
        common = layui.common,
        table  = layui.table ;
    table.render({
        elem: '#paper'
        ,height: 'full-200'
        ,method:'GET'
        ,url: '/admin/answer/list' //数据接口
        ,page: true //开启分页
        ,cols: [[ //表头
            {type: 'checkbox', align:'center',unresize:true}
            ,{field: 'paperid', align:'center', title: '问卷编号',unresize:true,templet:'<div>{{d.paper.num}}</div>'}
            ,{field: 'num', align:'center', title: '学生编号',unresize:true}
            ,{field: 'questionType', align:'center', title: '题目类型',unresize:true}
            ,{field: 'createDate', align:'center', title: '创建时间',unresize:true}
            ,{field: 'updateDate', align:'center', title: '更新时间',unresize:true}
            ,{field: 'answerContent', align:'center', title: '提交答案',unresize:true}
            ,{field: 'questionTitle', align:'center', title: '题目',unresize:true,templet:'<div>{{d.question.questionTitle}}</div>'}
            ,{field: 'questionOption', align:'center', title: '题目选项',unresize:true,templet:'<div>{{d.question.questionOption}}</div>'}
            ,{field: 'questionAnswer', align:'center', title: '正确答案',unresize:true,templet:'<div>{{d.question.questionAnswer}}</div>'}
        ]]
    });
    //监听工具条
    //分页
    laypage.render({
        elem: 'pageDemo' //分页容器的id
        ,count: 100 //总页数
        ,skin: '#1E9FFF' //自定义选中色值
        //,skip: true //开启跳页
        ,jump: function(obj, first){
            if(!first){
                layer.msg('第'+ obj.curr +'页');
            }
        }
    });
    //输出接口，主要是两个函数，一个删除一个编辑
    var datalist = {
        deleteData: function (id) {
            layer.confirm('确定删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                del(id);
            }, function () {

            });
        },
        editData: function (id) {
            common.frame_show('分类编辑','/admin/paper/form?id='+id);
        }
    };
    function del(id) {
        layer.confirm('真的删除行么', function (index) {
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: "/admin/paper/" + id + "/del",
                success: function (ret) {
                    if (ret.isOk) {
                        layer.msg("操作成功", {time: 2000}, function () {
                            layer.close(index);
                            window.location.href = "/admin/paper/index";
                        });
                    } else {
                        layer.msg(ret.msg, {time: 2000});
                    }
                }
            });
        });
    }

    exports('answer/index', datalist);
});
