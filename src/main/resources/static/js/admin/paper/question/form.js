layui.define(['element', 'layer', 'form','laydate'], function (exports) {
    var form = layui.form;
    var $ = layui.jquery;
    var laydate  = layui.laydate;
    //自定义验证

    var paperid = $('#paperid').val();
    var questionid = $('#questionid').val();

    form.verify({
        name: function (value) {
            if (value.length <= 0 || value.length > 10) {
                return "账号必须1到10位"
            }
        }

    });
    Date.prototype.Format = function(fmt)
    { //author: meizz
        var o = {
            "M+" : this.getMonth()+1,                 //月份
            "d+" : this.getDate(),                    //日
            "h+" : this.getHours(),                   //小时
            "m+" : this.getMinutes(),                 //分
            "s+" : this.getSeconds(),                 //秒
            "q+" : Math.floor((this.getMonth()+3)/3), //季度
            "S"  : this.getMilliseconds()             //毫秒
        };
        if(/(y+)/.test(fmt))
            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
        for(var k in o)
            if(new RegExp("("+ k +")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        return fmt;
    }

    //监听登陆提交
    form.on('submit(add)', function (data) {
        console.log(data.elem);//被执行事件的元素DOM对象，一般为button对象
        console.log(data.form);//被执行提交的form对象，一般在存在form标签时才会返回
        console.log(data.field);//当前容器的全部表单字段，名值对形式：{name: value}
        data.field.id = questionid;
        data.field.paper = paperid;
        data.field.createTime = (new Date()).Format("yyyy-MM-dd hh:mm:ss");

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/admin/paper/question/save",
                data: data.field,
                success: function(ret){
                    console.log(ret);
                    if(ret.isOk){
                        layer.msg("操作成功", {time: 2000},function(){
                            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                            parent.layer.close(index);
                            window.parent.location.href="/admin/paper/question/index";
                        });
                    }else{
                        layer.msg(ret.msg, {time: 2000});
                    }
                }
            });
        return false;
    });
    exports('paper/question/form', {});
});

