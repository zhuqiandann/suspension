layui.define(['laypage', 'layer',  'table','common','util','form'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        util = layui.util,
        laypage = layui.laypage,
        form = layui.form,
        common = layui.common,
        table  = layui.table ;

    var paperid = $('#paperid').val();
    console.log('Question/index',paperid)

    table.render({
        elem: '#question'
        ,height: 'full-200'
        ,method:'GET'
        ,url: `/admin/paper/question/idlist?id=${paperid}` //数据接口
        ,page: true //开启分页
        ,cols: [[ //表头
            {type: 'checkbox', align:'center',unresize:true}
            ,{field: 'paperid', align:'center', title: '问卷编号',unresize:true,templet:'<div>{{d.paper.num}}</div>'}
            ,{field: 'questionTitle', align:'center', title: '问卷题目',unresize:true}
            ,{field: 'createTime', align:'center', title: '创建时间',unresize:true}
            ,{field: 'questionType', align:'center', title: '题目类型',unresize:true}
            ,{field: 'questionOption', align:'center', title: '选项',unresize:true}
            ,{field: 'questionAnswer', align:'center', title: '答案',unresize:true}
            ,{fixed: 'right',  title:'操作',align:'center', toolbar: '#operator',unresize:true}
        ]]
    });
    //监听工具条
    table.on('tool(table)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            del(data.id);
        } else if(obj.event === 'edit'){
            w=($(window).width()*0.9); //edis
            h=($(window).height() - 50);
            layer.open({
                type: 2,
                area: [w+'px', h +'px'],
                fix: false, //不固定
                maxmin: true,
                shadeClose: true,
                shade:0.4,
                title: "编辑问题",
                content:'/admin/paper/question/form?id='+data.id,
                success:function (layero,index) {
                    var body = layer.getChildFrame('body',index);
                    body.contents().find("#paperid").val(paperid);
                }
            });
        }
    });
    //分页
    laypage.render({
        elem: 'pageDemo' //分页容器的id
        ,count: 100 //总页数
        ,skin: '#1E9FFF' //自定义选中色值
        //,skip: true //开启跳页
        ,jump: function(obj, first){
            if(!first){
                layer.msg('第'+ obj.curr +'页');
            }
        }
    });


    //添加数据
    $('#addDept').click(function () {
        var index = layer.load(1);
        setTimeout(function () {
            layer.close(index);
            w=($(window).width()*0.9);
            h=($(window).height() - 50);
            layer.open({
                type: 2,
                area: [w+'px', h +'px'],
                fix: false, //不固定
                maxmin: true,
                shadeClose: true,
                shade:0.4,
                title: "编辑问题",
                content:'/admin/paper/question/form',
                success:function (layero,index) {
                    var body = layer.getChildFrame('body',index);
                    body.contents().find("#paperid").val(paperid);
                }
            });
            // layer.msg('打开添加窗口');
        }, 500);
    });

    //批量删除数据
    $('#deleteAll').click(function () {
        var index = layer.load(1);

    });

    //输出接口，主要是两个函数，一个删除一个编辑
    var datalist = {
        deleteData: function (id) {
            layer.confirm('确定删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                del(id);
            }, function () {

            });
        },
        editData: function (id) {
            common.frame_show('分类编辑','/admin/paper/question/form?id='+id);
        }
    };
    function del(id) {
        layer.confirm('真的删除行么', function (index) {
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: "/admin/paper/question/" + id + "/del",
                success: function (ret) {
                    if (ret.isOk) {
                        layer.msg("操作成功", {time: 2000}, function () {
                            layer.close(index);
                            window.location.href = "/admin/paper/question/index";
                        });
                    } else {
                        layer.msg(ret.msg, {time: 2000});
                    }
                }
            });
        });
    }


    exports('paper/question/index', datalist);
});
