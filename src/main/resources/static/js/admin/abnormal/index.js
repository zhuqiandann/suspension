layui.define([ 'layer',  'table','common'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        common = layui.common,
        table  = layui.table ;
    table.render({
        elem: '#abnormal'
        ,height: 'full-200'
        ,method:'GET'
        ,url: '/admin/abnormal/list' //数据接口
        ,page: true //开启分页
        ,cols: [[ //表头
            {type: 'checkbox', align:'center',unresize:true}
            ,{field: 'name', align:'center', title: '课程名称',unresize:true}
            ,{field: 'teacherName', align:'center', title: '任课老师名字',unresize:true}
            ,{field: 'num', align:'center', title: '任课老师编号',unresize:true}
            ,{field: 'week', align:'center', title: '星期',unresize:true}
            ,{field: 'section', align:'center', title: '节数',unresize:true}
            ,{field: 'updateDate', align:'center', title: '上报时间',unresize:true}
            ,{field: 'description', align:'center', title: '问题描述',unresize:true}
            ,{fixed: 'right',  title:'操作',align:'center', toolbar: '#operator',unresize:true}
        ]]
    });

    //监听工具条
    table.on('tool(table)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            del(data.id);
        }
    });
    /*

        //添加数据
        $('#addStudent').click(function () {
            var index = layer.load(1);
            setTimeout(function () {
                layer.close(index);
                common.frame_show('分类添加','/admin/student/form');
                // layer.msg('打开添加窗口');
            }, 500);
        });
        //批量删除数据
        $('#deleteAll').click(function () {
            var index = layer.load(1);

        });

        var dept,keyword,major='';
        $('#search').click(function () {
            keyword = $("#keyword").val();
            table.reload('student', {
                url: "/student/search"
                ,where: {keyword:keyword} //设定异步数据接口的额外参数
                // ,where: {keyword:keyword,dept:dept,major:major} //设定异步数据接口的额外参数
                //,height: 300
            });
        });
    */
    //输出接口，主要是两个函数，一个删除一个编辑
    var datalist = {
        deleteData: function (id) {
            layer.confirm('确定删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                del(id);
            }, function () {

            });
        }
    };
    function del(id) {
        layer.confirm('真的处理完了么', function (index) {
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: "/admin/abnormal/" + id + "/del",
                success: function (ret) {
                    if (ret.isOk) {
                        layer.msg("操作成功", {time: 2000}, function () {
                            layer.close(index);
                            window.location.href = "/admin/abnormal/index";
                        });
                    } else {
                        layer.msg(ret.msg, {time: 2000});
                    }
                }
            });
        });
    }
    exports('admin/abnormal/index', datalist);
});