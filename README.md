#### 项目部署

#### 本项目使用Springboot+Mysql+Flyway+Layui

>  *  安装jdk和mysql环境。
>  *  将代码clone下来后，用idea打开。</br>
>  *  修改数据库配置，帐号密码。
>  *  如果是首次运行，先在数据库里面创建一个数据库scis。
>  *  运行成功后访问http://localhost:8080/
#### 登陆页面
![img_2.png](img_2.png)

#### 首页
![img.png](img.png)
